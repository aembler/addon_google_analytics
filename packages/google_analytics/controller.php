<?php
namespace Concrete\Package\GoogleAnalytics;

use Concrete\Core\Database\EntityManager\Provider\ProviderAggregateInterface;
use Concrete\Core\Database\EntityManager\Provider\StandardPackageProvider;
use Concrete\Core\Package\Package;
use PortlandLabs\GoogleAnalytics\AnalyticsServiceProvider;

class Controller extends Package implements ProviderAggregateInterface
{
    protected $pkgHandle = 'google_analytics';
    protected $appVersionRequired = '8.0.3';
    protected $pkgVersion = '0.6';
    protected $pkgAutoloaderMapCoreExtensions = true;
    protected $pkgAutoloaderRegistries = array(
        'src/PortlandLabs/GoogleAnalytics' => '\PortlandLabs\GoogleAnalytics',
    );

    public function getPackageDescription()
    {
        return t("Adds Google Analytics to your concrete5 Site.");
    }

    public function getPackageName()
    {
        return t("Google Analytics");
    }

    public function getEntityManagerProvider()
    {
        $provider = new StandardPackageProvider($this->app, $this, [
        ]);
        return $provider;
    }

    public function validate_install()
    {
        $e = $this->app->make('error');
        return $e;
    }

    public function upgrade()
    {
        parent::upgrade();
        $this->installContentFile('content.xml');
    }

    public function install()
    {
        $pkg = parent::install();
        $this->on_start();
        $this->installContentFile('content.xml');
    }

    public function on_start()
    {
        require_once $this->getPackagePath() . '/vendor/autoload.php';
        $pkg = \Package::getByHandle($this->pkgHandle);
        $provider = new AnalyticsServiceProvider($this->app, $pkg);
        $provider->register();
    }

}
