<?php defined('C5_EXECUTE') or die("Access Denied.");?>

<h4><?=t('Connection Status')?></h4>

<?php

if ($connectionError->has()) { ?>

    <div class="alert alert-danger">
        <?php
            $formatter = new \Concrete\Core\Error\ErrorList\Formatter\StandardFormatter($connectionError);
            print $formatter->render();
        ?>
    </div>
<?php } else { ?>

    <div class="alert alert-success"><?=$status?></div>

<?php } ?>
