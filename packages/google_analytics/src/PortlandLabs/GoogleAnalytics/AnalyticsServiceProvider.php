<?php
namespace PortlandLabs\GoogleAnalytics;

use Concrete\Core\Foundation\Service\Provider as BaseServiceProvider;
use Krizon\Google\Analytics\MeasurementProtocol\MeasurementProtocolClient;
use PortlandLabs\GoogleAnalytics\MeasurementProtocol\Tracker;
use PortlandLabs\GoogleAnalytics\Utility\CookieParser;
use TheIconic\Tracking\GoogleAnalytics\Analytics;

class AnalyticsServiceProvider extends BaseServiceProvider
{

    public function register()
    {
        $app = $this->app;
        $this->app->singleton('google_analytics/reporting', function() use ($app) {
            $pkg = \Package::getByHandle('google_analytics');
            $config = $pkg->getFileConfig()->get('credentials.reporting');
            if (empty($config)) {
                throw new \Exception('You must populate the file application/config/google_analytics/credentials.php with an array containing your service account credentials.');
            }

            // Create and configure a new client object.
            $client = new \Google_Client();
            $client->setApplicationName("Google Analytics concrete5");
            $client->setAuthConfig($config);
            $client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
            $analytics = new \Google_Service_Analytics($client);
            return $analytics;
        });

        $this->app->singleton('google_analytics/tracking', function() use ($app) {
            $pkg = \Package::getByHandle('google_analytics');
            $config = $pkg->getFileConfig()->get('credentials.tracking');
            $tracking = new Tracker($config, $this->app->make('cookie'));
            return $tracking;
        });
    }


}
