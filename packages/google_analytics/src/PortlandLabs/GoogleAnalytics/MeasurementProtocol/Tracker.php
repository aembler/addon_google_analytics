<?php
namespace PortlandLabs\GoogleAnalytics\MeasurementProtocol;

use Concrete\Core\Cookie\CookieJar;
use TheIconic\Tracking\GoogleAnalytics\Analytics;
use Wb\GoogleAnalyticsCookieParser\GoogleAnalyticsCookieParser;
use Wb\GoogleAnalyticsCookieParser\ParsedCookie;

class Tracker extends Analytics
{

    public function __construct($config, CookieJar $cookie)
    {
        $parser = new GoogleAnalyticsCookieParser();
        $clientId = null;
        /**
         * @var $cookie ParsedCookie
         */
        try {
            $cookie = $parser->parse($cookie->get('_ga'));
            $clientId = $cookie->getClientId();
        } catch (\Exception $e) {
        }
        if (!$clientId) {
            $clientId = $this->generateClientId();
        }

        $isSSL = $config['ssl'] ? true : false;
        parent::__construct($isSSL);
        $this->setTrackingId($config['tid']);
        $this->setClientId($clientId);
        $this->setProtocolVersion(1);
    }

    protected function generateClientId()
    {
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

}
