<?php
namespace Concrete\Package\GoogleAnalytics\Controller\SinglePage\Dashboard\System\GoogleAnalytics;

use Concrete\Core\Page\Controller\DashboardPageController;
use Concrete\Core\Site\Service;
use Concrete\Package\Msm\Controller\Element\Dashboard\Site\Menu;
use PortlandLabs\GoogleAnalytics\Analytics;

class Setup extends DashboardPageController
{

    public function view()
    {

        $connectionError = $this->app->make('error');
        try {
            $analytics = $this->app->make('google_analytics/reporting');
            $accounts = $analytics->management_accounts->listManagementAccounts();
            if (count($accounts->getItems()) > 0) {
                $items = $accounts->getItems();
                $account = $items[0]->getId();
                $this->set('status', t('Successfully connected to %s', $account));
            }
        } catch (\Exception $e) {
            $connectionError->add($e);
        }

        $this->set('connectionError', $connectionError);
    }


}
