<?php
namespace Concrete\Package\GoogleAnalytics\Controller\SinglePage\Dashboard\System;

use Concrete\Core\Page\Controller\DashboardPageController;

class GoogleAnalytics extends DashboardPageController
{
    public function view()
    {
        $this->redirect('/dashboard/system/google_analytics/setup');
    }
}
